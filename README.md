![builddoc-banner](./docs/assets/builddoc-bg.png)
<p align="center">
	<a href="https://github.com/itsamedood/BuildDoc/blob/main/LICENSE">
		<img src="https://img.shields.io/github/license/itsamedood/BuildDoc?color=blue&style=for-the-badge">
	</a>
	<a href="https://github.com/itsamedood/BuildDoc">
		<img src="https://img.shields.io/lgtm/grade/python/github/itsamedood/BuildDoc?style=for-the-badge">
	</a>
	<a href="https://github.com/itsamedood/BuildDoc">
		<img src="https://img.shields.io/github/stars/itsamedood/BuildDoc?style=for-the-badge">
	</a>
</p>

# **BuildDoc Repository**
- ### [Documentation](./docs/00-Welcome.md)
- ### [Basic Installation](#basic-installation)

# Basic Installation
> \* *For a more detailed installation, such as installing from source, please see the Documentation.*
>
> ## Steps
> - Go to [releases](https://github.com/itsamedood/BuildDoc/releases/).
---
